package kata.checkout;

import java.util.stream.Stream;
import org.junit.Test;

import static org.junit.Assert.*;

public class CheckoutTest {

    Object rules;

    @Test
    public void checkoutComputesTotalPrice() {
        assertEquals(  0, price(""));
        assertEquals( 50, price("A"));
        assertEquals( 80, price("AB"));
        assertEquals(115, price("CDBA"));

        assertEquals(100, price("AA"));
        assertEquals(130, price("AAA"));
        assertEquals(180, price("AAAA"));
        assertEquals(230, price("AAAAA"));
        assertEquals(260, price("AAAAAA"));

        assertEquals(160, price("AAAB"));
        assertEquals(175, price("AAABB"));
        assertEquals(190, price("AAABBD"));
        assertEquals(190, price("DABABA"));
    }

    private int price(String goods) {
        Checkout checkout = Checkout.newInstance(rules);
        split(goods).forEach(checkout::scan);
        return checkout.total();
    }

    private Stream<String> split(String s) {
        return s.chars().mapToObj(c -> String.valueOf((char) c));
    }
}
