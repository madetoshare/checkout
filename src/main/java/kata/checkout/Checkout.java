package kata.checkout;

public class Checkout {

    public void scan(String sku) {
        throw new UnsupportedOperationException("Implement me!");
    }

    public int total() {
        throw new UnsupportedOperationException("Implement me!");
    }

    /*
     * The rules argument is just a placeholder.
     * You are free to use whatever data structure fits the purpose.
     */
    public static Checkout newInstance(Object rules) {
        throw new UnsupportedOperationException("Implement me!");
    }
}
